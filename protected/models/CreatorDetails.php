<?php

/**
 * This is the model class for table "he_creator_details".
 *
 * The followings are the available columns in table 'he_creator_details':
 * @property integer $id
 * @property integer $user_id
 * @property string $identity_name
 * @property string $place_of_origin
 * @property string $license_type
 * @property string $copyright
 * @property string $tag_similar_items
 */
class CreatorDetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CreatorDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'he_creator_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, identity_name, place_of_origin', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('identity_name', 'length', 'max'=>50),
			array('place_of_origin', 'length', 'max'=>30),
			array('license_type, copyright, tag_similar_items', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, identity_name, place_of_origin, license_type, copyright, tag_similar_items', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'identity_name' => 'Identity Name',
			'place_of_origin' => 'Place Of Origin',
			'license_type' => 'License Type',
			'copyright' => 'Copyright',
			'tag_similar_items' => 'Tag Similar Items',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('identity_name',$this->identity_name,true);
		$criteria->compare('place_of_origin',$this->place_of_origin,true);
		$criteria->compare('license_type',$this->license_type,true);
		$criteria->compare('copyright',$this->copyright,true);
		$criteria->compare('tag_similar_items',$this->tag_similar_items,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
