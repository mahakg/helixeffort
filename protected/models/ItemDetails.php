<?php

/**
 * This is the model class for table "he_item_details".
 *
 * The followings are the available columns in table 'he_item_details':
 * @property integer $id
 * @property integer $user_id
 * @property string $item_name
 * @property string $item_desc
 * @property string $release_date
 * @property integer $item_category
 * @property string $audience_drive
 * @property string $licence_type
 * @property string $copyright
 */
class ItemDetails extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ItemDetails the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'he_item_details';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id, item_name, release_date, item_category', 'required'),
			array('user_id, item_category', 'numerical', 'integerOnly'=>true),
			array('item_name, audience_drive', 'length', 'max'=>100),
			array('item_desc', 'length', 'max'=>2048),
			array('licence_type, copyright', 'length', 'max'=>200),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, user_id, item_name, item_desc, release_date, item_category, audience_drive, licence_type, copyright', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'item_name' => 'Item Name',
			'item_desc' => 'Item Desc',
			'release_date' => 'Release Date',
			'item_category' => 'Item Category',
			'audience_drive' => 'Audience Drive',
			'licence_type' => 'Licence Type',
			'copyright' => 'Copyright',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('item_desc',$this->item_desc,true);
		$criteria->compare('release_date',$this->release_date,true);
		$criteria->compare('item_category',$this->item_category);
		$criteria->compare('audience_drive',$this->audience_drive,true);
		$criteria->compare('licence_type',$this->licence_type,true);
		$criteria->compare('copyright',$this->copyright,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
