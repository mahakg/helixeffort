<?php

/**
 * This is the model class for table "he_randomlookups".
 *
 * The followings are the available columns in table 'he_randomlookups':
 * @property integer $id
 * @property string $random_num
 * @property integer $user_id
 * @property string $date_added
 * @property string $activity_name
 */
class Randomlookups extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Randomlookups the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'he_randomlookups';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('random_num, user_id, date_added, activity_name', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('random_num', 'length', 'max'=>32),
			array('activity_name', 'length', 'max'=>10),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, random_num, user_id, date_added, activity_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'random_num' => 'Random Num',
			'user_id' => 'User',
			'date_added' => 'Date Added',
			'activity_name' => 'Activity Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('random_num',$this->random_num,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('activity_name',$this->activity_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}