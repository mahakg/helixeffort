<?php

/**
 * This is the model class for table "he_queue".
 *
 * The followings are the available columns in table 'he_queue':
 * @property integer $id
 * @property string $senderemail
 * @property string $sendername
 * @property string $recpemail
 * @property string $subject
 * @property string $message
 * @property string $error
 * @property string $createdon
 * @property string $updatedon
 * @property string $attachments
 * @property string $cc
 * @property string $bcc
 */
class Queue extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Queue the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'he_queue';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('senderemail, sendername, recpemail, subject, message, createdon, updatedon, attachments', 'required'),
			array('senderemail, sendername, recpemail', 'length', 'max'=>100),
			array('subject, cc, bcc', 'length', 'max'=>255),
			array('message', 'length', 'max'=>8192),
			array('error', 'length', 'max'=>1),
			array('attachments', 'length', 'max'=>2048),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, senderemail, sendername, recpemail, subject, message, error, createdon, updatedon, attachments, cc, bcc', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'senderemail' => 'Senderemail',
			'sendername' => 'Sendername',
			'recpemail' => 'Recpemail',
			'subject' => 'Subject',
			'message' => 'Message',
			'error' => 'Error',
			'createdon' => 'Createdon',
			'updatedon' => 'Updatedon',
			'attachments' => 'Attachments',
			'cc' => 'Cc',
			'bcc' => 'Bcc',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('senderemail',$this->senderemail,true);
		$criteria->compare('sendername',$this->sendername,true);
		$criteria->compare('recpemail',$this->recpemail,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('error',$this->error,true);
		$criteria->compare('createdon',$this->createdon,true);
		$criteria->compare('updatedon',$this->updatedon,true);
		$criteria->compare('attachments',$this->attachments,true);
		$criteria->compare('cc',$this->cc,true);
		$criteria->compare('bcc',$this->bcc,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}