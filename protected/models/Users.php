<?php

/**
 * This is the model class for table "he_users".
 *
 * The followings are the available columns in table 'he_users':
 * @property integer $id
 * @property string $name
 * @property string $association
 * @property string $email
 * @property string $password
 * @property string $username
 * @property string $circle_beneficiary
 * @property string $date_added
 * @property string $last_modified
 * @property string $status
 * @property string $signin_expiry
 * @property string $timezone
 * @property string $about
 * @property string $profile_image
 * @property string $last_login
 * @property string $user_type
 * @property string $user_country
 * @property string $reset_session
 */
class Users extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'he_users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, association, email, password, username, circle_beneficiary, date_added, last_modified, status', 'required'),
			array('name, email, circle_beneficiary, profile_image', 'length', 'max'=>50),
			array('association, password, timezone', 'length', 'max'=>30),
			array('username', 'length', 'max'=>45),
			array('status, reset_session', 'length', 'max'=>1),
			array('user_type, user_country', 'length', 'max'=>250),
			array('signin_expiry, about, last_login', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, association, email, password, username, circle_beneficiary, date_added, last_modified, status, signin_expiry, timezone, about, profile_image, last_login, user_type, user_country, reset_session', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'association' => 'Association',
			'email' => 'Email',
			'password' => 'Password',
			'username' => 'Username',
			'circle_beneficiary' => 'Circle Beneficiary',
			'date_added' => 'Date Added',
			'last_modified' => 'Last Modified',
			'status' => 'Status',
			'signin_expiry' => 'Signin Expiry',
			'timezone' => 'Timezone',
			'about' => 'About',
			'profile_image' => 'Profile Image',
			'last_login' => 'Last Login',
			'user_type' => 'User Type',
			'user_country' => 'User Country',
			'reset_session' => 'Reset Session',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('association',$this->association,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('circle_beneficiary',$this->circle_beneficiary,true);
		$criteria->compare('date_added',$this->date_added,true);
		$criteria->compare('last_modified',$this->last_modified,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('signin_expiry',$this->signin_expiry,true);
		$criteria->compare('timezone',$this->timezone,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('profile_image',$this->profile_image,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('user_type',$this->user_type,true);
		$criteria->compare('user_country',$this->user_country,true);
		$criteria->compare('reset_session',$this->reset_session,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}