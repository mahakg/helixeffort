<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	protected $_id;
	
	public $user;
	public $is_sso;
	public $user_email;
	
	const ERROR_ACCOUNT_INACTIVE = -10;
	const ERROR_ACCOUNT_EXPIRED = -1;
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$user_name=strtolower($this->username);
		$criteria = new CDbCriteria;
		$criteria->select = array('id', 'username', 'password', 'name', 'association', 'email', 'profile_image', 'circle_beneficiary', 'timezone', 'date_added, last_modified, status, signin_expiry, reset_session', 'user_type');

		// If SSO, then use a different criteria
		if ($this->is_sso) {
		$criteria->condition = 'LOWER(useremail) = :useremail';
		$criteria->params = array(':useremail'=>$this->useremail);
		} else {
		$criteria->condition = '(LOWER(username) = :username OR LOWER(email) = :username) and password = password(:password)';
		$criteria->params = array(':username' => $this->username, ':password'=>$this->password);
		}

		//$record = User::model()->find('LOWER(user_name)=?',array($user_name));
		$record = Users::model()->find($criteria);
		
		if($record===null)
		{
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		else if( $record->status=='e')
		{
			$this->errorCode=self::ERROR_ACCOUNT_EXPIRED;
		}
		else if( $record->status=='i')
		{
			$this->errorCode=self::ERROR_ACCOUNT_INACTIVE;
		}
		else
		{                        
			$this->_id=$record->id;
			
			$profile = new stdClass();
			$profile->id = $record->id;
			$profile->username = $record->username;
			$profile->name = $record->name; 
			$profile->email = $record->email;
			$profile->association = $record->association;
			$profile->circle_beneficiary = $record->circle_beneficiary;
			$profile->user_type = $record->user_type;
			
			
			//$repositoryPath = Yii::app()->params['attachment'];
                        //$profilepicPath = Yii::app()->params['profilepic'];
                        //$profile->profile_image_path = '../'.$profilepicPath.$record->client_id.'/';
			//$profile->profile_image = '../'.$profilepicPath.$record->client_id.'/'.$record->profile_image;
			//$profile->designation = $record->designation; // last modified by Vir on 28 Nov 2012
			$profile->timezone = ($record->timezone==''?'Asia/Kolkata':$record->timezone);
			//$profile->language = $record->language;
			$profile->language = 'en';
			//$profile->user_display_name = $record->user_fname." ".$record->user_lname;// added by Vir on 28 Nov 2012 //last modified by Vir on 25 Feb 2013
		/*	$profile->is_sso = 'n'; // Added by Vivek for SSO validation on 07 March 2013
			if ($this->is_sso) {
			$profile->is_sso = 'y';
			}
		*/	
			$this->setState('profile', $profile);
			$this->errorCode=self::ERROR_NONE;
		}
		return $this->errorCode;
	}
	
	public function getId()
	{
		return $this->_id;
	}

	public function getUser()
	{
		return $this->user;
	}
 
    public function setUser(CActiveRecord $user)
    {
        $this->user = $user->attributes;
    }
}
