<?php

class Utility
{
	public function safe($data, $connect)
	{
		$conn = Yii::app()->db;
		
		return mysql_real_escape_string($data,$conn);//$connect->real_escape_string($data);
	}
	
	public function vedate($datestring, $datestyle="", $reverse='', $currenttz = "", $requiredtz = "")
	{
	        if ($datestyle == "") { $datestyle = 'small2'; }
	
	        $currenttz1 = $currenttz;
	        $requiredtz1 = $requiredtz;
	        if ($reverse != '') {
	                $currenttz1 = $requiredtz;
	                $requiredtz1 = $currenttz;
	        }
	
		if($requiredtz1 != "")
	        	date_default_timezone_set($requiredtz1);
	
	        $datestring = "$datestring $currenttz1";
	        
		$month_diff=floor((strtotime(date("Y-m-d H:i:s"))-strtotime($datestring))/(60 * 60 * 24 * 30));
		
		if ($month_diff<11 && $datestyle == 'small3') {
			$datestyle = 'small4';
		}

	        switch ($datestyle)
	                {       //Wednesday, 07 Oct 09, 17:08 IST (+0530)
	                	case 'small': $date = strftime("%d %b %y %H:%M",strtotime($datestring));break;
	                        case 'small2': $date = strftime("%d %b %Y, %H:%M",strtotime($datestring));break;
	                        case 'small3': $date = strftime("%d/%b/%y",strtotime($datestring));break;
	                        case 'small4': $date = strftime("%d/%b",strtotime($datestring));break;
	                        case 'smallwithtz1': $date = strftime("%d %b %y %H:%M %Z",strtotime($datestring)); break;
	                        case 'smallwithtz2': $date = strftime("%d %b %y %H:%M %Z (%z)",strtotime($datestring)); break;
	                        case 'smallwithtz212h': $date = strftime("%d %b %y %I:%M %p %Z (%z)",strtotime($datestring)); break;
	                        case 'medium': $date = strftime("%d %b %y, %H:%M %Z (%z)",strtotime($datestring)); break;
	                        case 'big': $date = strftime("%d %b %Y, %H:%M %Z (%z)",strtotime($datestring)); break;
	                        case 'bigwithday': $date = strftime("%A, %d %B %Y, %H:%M %Z",strtotime($datestring)); break;
	                        case 'bigwithdaytz': $date = strftime("%A, %d %B %y, %H:%M %Z (%z)",strtotime($datestring)); break;
	                        case 'timestamp': $date = strftime("%Y-%m-%d %H:%M:%S",strtotime($datestring)); break;
	                        case 'fulldate': $date = strftime("%d-%b-%Y",strtotime($datestring));break;
	                        case 'fulldate2': $date = strftime("%d-%b-%y",strtotime($datestring));break;
	                        case 'justdate': $date = strftime("%d",strtotime($datestring)); break;
	                        case 'justmonth': $date = strftime("%m",strtotime($datestring)); break;
	                        case 'justyear': $date = strftime("%Y",strtotime($datestring)); break;
	                        case 'justdatemonth': $date = strftime("%d %m",strtotime($datestring)); break;
	                        case 'onlytime': $date = strftime("%H:%M:%S",strtotime($datestring)); break;
	                        case 'onlytime12h': $date = strftime("%I:%M:%S",strtotime($datestring)); break;
	                        case 'ampm': $date = strftime("%p",strtotime($datestring)); break;
	                        case 'd/m/Y': $date = strftime("%d/%m/%Y",strtotime($datestring)); break; // added by Vir on 6 Feb 2013
				case 'd-M-Y': $date = strftime("%d-%b-%Y",strtotime($datestring)); break; // added by Vir on 6 Feb 2013
	                        case 'javascript': $date = strftime("%a %b %d %Y %H:%M:%S GMT%z (%Z)",strtotime($datestring)); break;
	                        default: $date = strftime("%d %b %y, %H:%M %Z (%z)",strtotime($datestring)); break;
	                }
	                
	                if($currenttz != "")
	        		date_default_timezone_set($currenttz);
	
	        return $date;
	}
	
	public function stripContent($content)			
	{	
		$content = trim($content);
		if(empty($content))
		{
			return $content;
		}	

		//$content = utf8_encode(trim($content));
				
		$allow_tags_array = array('p', 'div', 'span', 'table', 'tr', 'th', 'td', 'tbody', 'thead', 'tfooter', 'b', 'strong', 'i', 'em', 'u', 'strike', 'li', 'ol', 'ul', 'blockquote', 'code', 'a','br','pre','font', 'script');
		
		$allowed_tags = '';
		foreach($allow_tags_array as $allow_tag)
		{
			$allowed_tags .= '<'.$allow_tag.'>';
		}	
		
		//print_r($allow_tags_array);die;
		//$content = self::strip_html_tags($content);	
		$content = strip_tags($content, $allowed_tags);
		
		$okattrs = array( 
                'a' => array('href' => true), 
		'table' => array('border'=> true),
		'td' => array('colspan'=> true),
		'font' => array('style'=> true)
            	);
      
		foreach($allow_tags_array as $allow_tag)
		{
			switch($allow_tag)
			{
				case 'a':
					$doc = new DomDocument();
					@$doc->loadHTML('<?xml encoding="UTF-8">'.$content);

					$a_tags = $doc->getElementsByTagName($allow_tag);
					foreach($a_tags as $a_tag_obj)
					{ 
						$attribute_list = array();
						foreach($a_tag_obj->attributes as $name => $attrNode)
						{
							if (!isset($okattrs[$a_tag_obj->nodeName][$name]))
							{
								$attribute_list[] = $name;
							}	
						}
						for($i=0;$i<sizeof($attribute_list);$i++)
						{
							$a_tag_obj->removeAttribute($attribute_list[$i]);
						}	
   				}
    							
    				$content = $doc->saveHTML();
					
					break;
				case 'table' :
					$doc = new DomDocument();
					@$doc->loadHTML('<?xml encoding="UTF-8">'.$content);
					
					$table_tags = $doc->getElementsByTagName($allow_tag);
					foreach($table_tags as $table_tag_obj)
					{ 
						$attribute_list = array();
						foreach($table_tag_obj->attributes as $name => $attrNode)
						{
							if (!isset($okattrs[$table_tag_obj->nodeName][$name]))
							{
								$attribute_list[] = $name;
							}	
						}
						for($i=0;$i<sizeof($attribute_list);$i++)
						{
							$table_tag_obj->removeAttribute($attribute_list[$i]);
						}	
   				}
    					
					$content = $doc->saveHTML();
					break;
				case 'td' : 
					break;
                                case 'font' :
                                        $doc = new DomDocument();
                                        @$doc->loadHTML('<?xml encoding="UTF-8">'.$content);
                                        
                                        $font_tags = $doc->getElementsByTagName($allow_tag);
                                        foreach($font_tags as $font_tag_obj)
                                        { 
                                                $attribute_list = array();
                                                foreach($font_tag_obj->attributes as $name => $attrNode)
                                                {
                                                        if (!isset($okattrs[$font_tag_obj->nodeName][$name]))
                                                        {
                                                                $attribute_list[] = $name;
                                                        }       
                                                }
                                                for($i=0;$i<sizeof($attribute_list);$i++)
                                                {
                                                        $font_tag_obj->removeAttribute($attribute_list[$i]);
                                                }       
                                }
                                        
                                        $content = $doc->saveHTML();
                                        break;
				default:	
					$content = preg_replace("/<".$allow_tag." [^>]*?>/", "<".$allow_tag.">", $content);
					break;			
			}	
		}
		
		
		//remove Script Tag Added by Mahak on August 3
		$doc = new DOMDocument();
		$doc->loadHTML($content);
		$script_tags = $doc->getElementsByTagName('script');
		$length = $script_tags->length;

		for ($i = 0; $i < $length; $i++)
		{
		$script_tags->item(0)->parentNode->removeChild($script_tags->item(0));
		}

		// get the HTML string back
		$content = $doc->saveHTML();

		foreach($allow_tags_array as $allow_tag)
		{
			$content = preg_replace('#<'.$allow_tag.'>(&nbsp;|\s)*?</'.$allow_tag.'>#si', '', $content);	
		}
		//$content = preg_replace('#<script(.*?)>(.*?)</script>#is', '', $content);
		$content = strip_tags($content, $allowed_tags);
			
		return $content;
	}
	
	function timeDiff($time)
	{
		$tstamp = strtotime($time);
		$hours = ((time() - $tstamp) / 3600);
		$mins = floor($hours*60);
		$hours = floor($hours);
		$days = floor($hours/24);
		$months = floor($days/30);
		$year = floor($months/12);
		if($mins < 60 )
		{
			if($mins==1)
				return $mins.' min ago';
			else
				return $mins.' mins ago';
		}
		elseif($hours<24)
		{
			if($hours==1)
				return $hours.' day ago';
			else
				return $hours.' hours ago';
		}
		elseif($days<30)
		{
			if($days==1)
				return $days.' day ago';
			else
				return $days.' hours ago';
		}
		elseif($months<12)
		{
			if($months==1)
				return $months.' month ago';
			else
				return $months.' months ago';
		}
	}

	//-------Added by Vir on 31-10-2012--------
	public static function truncate_text($text, $words = 100, $add_more_text = '...', $add_more_link = '')
	{

		// trim text first
		$text = mb_substr($text,0,512);

		// split the string by spaces into an array
		$split = explode(' ',$text);
		
		if (sizeof($split) < $words) {
			return $text;
		}

		if (count($split)>$words) {
		// rebuild the excerpt back into a string
		$text = join(' ',array_slice($split,0,$words));
		}

		return $text . $add_more_text;
	}
	
	// find out the date difference
	public static function vedatediff($date1,$date2,$resulttype='')
	{
	        if ($resulttype == '') { $resulttype = 'days'; }
	        // make the first date
	        $datearrayA1 = explode(" ",$date1);
	        $datearrayA2 = explode("-",$datearrayA1[0]);
	        $datearrayA3 = explode(":",$datearrayA1[1]);
	        $dateA = mktime($datearrayA3[0],$datearrayA3[1],$datearrayA3[2],$datearrayA2[1],$datearrayA2[2],$datearrayA2[0]);
	
	        // make the second date
	        $datearrayB1 = explode(" ",$date2);
	        $datearrayB2 = explode("-",$datearrayB1[0]);
	        $datearrayB3 = explode(":",$datearrayB1[1]);
	        $dateB = mktime($datearrayB3[0],$datearrayB3[1],$datearrayB3[2],$datearrayB2[1],$datearrayB2[2],$datearrayB2[0]);
	
	        switch ($resulttype)
	        {
	                case 'years': $final = ($dateB-$dateA)/31536000; break;
	                case 'months': $final = ($dateB-$dateA)/2628000; break;
	                case 'days': $final = ($dateB-$dateA)/86400; break;
	                case 'hours': $final = ($dateB-$dateA)/3600; break;
	                case 'minutes': $final = ($dateB-$dateA)/60; break;
	                case 'seconds': $final = ($dateB-$dateA)/1; break;
	                case 'all':
				 $final = '';
				 $date_in_mk = $dateB-$dateA;
				  		
				 if(($date_in_mk)/31536000 > 1)
				 {
					if ((int)(($date_in_mk)/31536000) == '1'?$val='year':$val='years');
				 	$final .= (int)(($date_in_mk)/31536000)." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/31536000) * 31536000 );
				 }
				 else if(($date_in_mk)/2628000 > 1)
				 {
					if ((int)(($date_in_mk)/2628000) == '1'?$val='month':$val='months');
				 	$final .= (int)(($date_in_mk)/2628000)." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/2628000) * 2628000 );   
				 }
				 else if(($date_in_mk)/86400 > 1)
				 {
					if ((int)(($date_in_mk)/86400) == '1'?$val='day':$val='days');
				 	$final .= (int)($date_in_mk/86400)." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/86400) * 86400 );
				 	   
				 }
				 else if(($date_in_mk)/3600 > 1)
				 {
					if ((int)(($date_in_mk)/3600) == '1'?$val='hour':$val='hours');
				 	$final .= (int)(($date_in_mk)/3600)." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/3600) * 3600 );   
				 }
				 else if(($date_in_mk)/60 > 1)
				 {
					if ((int)(($date_in_mk)/60) == '1'?$val='minute':$val='minutes');
				 	$final .= (int)(($date_in_mk)/60)." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/60) * 60 );   
				 }
				 else if(($date_in_mk) > 0)
				 {
					if ((int)(($date_in_mk)) == '1'?$val='second':$val='seconds');
				 	$final .= (int)(($date_in_mk))." $val ";
				 	$date_in_mk -= ((int)($date_in_mk/60) * 60 );   
				 }
				 else if(($date_in_mk) == 0)
				 {
				 	$final .= " 0 second ";
				 }	
							 	                	 
							 

	                //case 'timeleft': $final = durationindhm($dateB-$dateA); if ($final == '') { $final=" - "; }break;
	        }
	        return $final;
	}
	
	//--------Added by Vir on 10-Dec-2012 to track IntegratePharma events logs--------
	// last modified by Vir on 29 Jan 2013
	public static function log_stats($contentId, $clientId, $moduleName, $userId, $activityName = '', $viaNewsletter = 'N')
	{
		if (!empty($_SERVER['HTTP_CLIENT_IP'])){
			$ip=$_SERVER['HTTP_CLIENT_IP'];
			//Is it a proxy address
		}elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
		  	$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}else{
		  	$ip=$_SERVER['REMOTE_ADDR'];
		}
		
		//The value of $ip at this point would look something like: "192.0.34.166"
		$locArr = array();	

		if(function_exists('geoip_record_by_name')) 
		{ 
			$locArr = @geoip_record_by_name($ip);
			$city = @$locArr[city];
			$country = @$locArr[country_name];
	 	} 
		else { 
			$city = 'New Delhi'; 
			$country = 'India'; 
		}

		
		if(!empty($_SERVER['HTTP_USER_AGENT']))
		{
			$browser = $_SERVER['HTTP_USER_AGENT'];
		}
		
		$IPLog = new IpLog;
		
		$IPLog['user_id'] = $userId; 
		$IPLog['client_id'] = $clientId;
		$IPLog['content_id'] = $contentId;
		$IPLog['date_added'] = new CDbExpression('now()');
		$IPLog['week_num'] = new CDbExpression('week(now())');
		$IPLog['month_num'] = new CDbExpression('month(now())');
		$IPLog['ipaddress'] = $ip; 
		$IPLog['browser'] = $browser; 
		$IPLog['country'] = $country; 
		$IPLog['city'] = $city; 
		$IPLog['module_name'] = $moduleName;
		$IPLog['activity_name'] = $activityName;
		$IPLog['via_newsletter'] = $viaNewsletter; // added by Vir on 29 Jan 2013
		
		if(Yii::app()->user->getState('isMobileTheme') == 'yes'){
			$IPLog['access_via'] = "mobile";
		}
			    		 	
	    	$IPLog->save(false);
		
		return true;
	}
	
	// To strip Google Alerts and similar stuff from Source Names
	function strip_unwantedsource($str) {
		$replace = array('via Google Alerts');
		return trim(str_replace($replace,'',$str));
	}
	
	//----Added by Vir on 07 Feb 2013--------Using in Statistics Module------------
        public static function getWeekStart($week_num,$year,$format = "d-M-Y")
	{	
		$week_start = new DateTime();
		$week_start->setISODate($year,$week_num);
		//$month=MonthName($month_num,False);
		return $week_start->format($format);
		//return $month;
	}

	public static function getMonthName($month_num)
	{
		return  date('F', mktime(0, 0, 0, $month_num, 1, date('Y')));
		
	}
	
	public static function email($from_name, $from_email,$to_email,$subject,$message,$raw="",$attachments="", $cc=array(),$bcc=array())
        {
                if ($raw != "") {
                	$message2 = $message;
		}
		else {
		        $message2 = "<span style='font-size:14px;font-family:arial,liberation sans;'>$message</span>"; // last modified by Vir on 11 Mar2013 - span tag closed
		        //$message2 = Utility::safe("<span style='font-size:14px;font-family:arial,liberation sans;'>$message</span><hr size=1 noshade><span style='font-family:arial,sans-serif;font-size:11px;'>This mail was intended for <B>$to_email</B>. You are receiving this email because you are a member of ".BRAND_NAME.". You can <a href=\"".WEB_PATH."/user/account\">sign in to your account</a> and change the frequency of emails.</span>", $db->getConnection());
		}
		
		if(is_array($cc) && sizeof($cc) > 0)
		{
			$cc_addrs = implode(',', $cc);
		}
		
		if(is_array($bcc) && sizeof($bcc) > 0)
		{
			$bcc_addrs = implode(',', $bcc);
		}
		
		$queue = new Queue;
		$mail = array();
		$mail['sendername'] = $from_name;
		$mail['senderemail'] = $from_email;
		$mail['recpemail'] = $to_email;
		$mail['subject'] = $subject;
		$mail['message'] = $message2;
		$mail['createdon'] = new CDbExpression('NOW()');
		$mail['attachments'] = $attachments;
		$mail['cc'] = $cc_addrs;
		$mail['bcc'] = $bcc_addrs;		
	       	
	       	$queue->attributes = $mail;
		$x = $queue->save();
		
	       	if($x){
	       		return true;
	       	}
       
        }
}




?>
