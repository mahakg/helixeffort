<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/layout-new';
	
	private $_assetsBase;
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	
	public function getAssetsBase()
	{
		$assetsPath = Yii::app()->params['assetsPath'];

		if ($this->_assetsBase === null) {

			// Done on 07 Dec 2012 by Vivek / Vir for setting static path in case it's a WAMP server as YII_DEBUG makes everything very slow
			if ($assetsPath != "") {
			$this->_assetsBase = $assetsPath;	
			}
			else {

				$this->_assetsBase = Yii::app()->assetManager->publish(
					Yii::getPathOfAlias('application.assets'),
					false,
					-1,
					YII_DEBUG
				);
			}
		}
		return $this->_assetsBase;
	}
	
	public function allowAccess($type,$priv) {

		// Widget Access is also used for top navigation
		$allowed_widgets = trim(Yii::app()->user->getState('client_widgets'));
		$feature_access = Yii::app()->user->getState('profile')->feature_access;
		if ($allowed_widgets != "") {
		$arr_widgets = explode(",",$allowed_widgets);
		} else {
		$arr_widgets = array();
		}

		if ($type == 'navigation') {

			if (in_array($priv,$arr_widgets)) { return true; } else { return false; }

		}
		else if ($type == 'feature') {
			if (in_array($priv,$feature_access)) { return true; } else { return false; }
		}
	}

        /*public function beforeAction()
        {
		if(!Yii::app()->user->isGuest){
			$module = $this->id;
			$action = $this->action->id;

			// Added by Vivek on 22 Nov 2012 to verify the privileges
			// Check current controller's access to user. Ignore default modules
			if ($module == 'site' || $module == 'auth') {
			
			} else {
			throw new CHttpException(403, 'You have no permission to view this content');
			return false;
			}
			
			exit;

			 $value = Yii::app()->request->cookies['PHPSESSID'];
			
			// Added a new variable in session "autologin" , it is done in order to check whether or not there is a cookie set for autologin or remember the session details.
			if ( yii::app()->user->getState('userSessionTimeout') < time() && !isset(yii::app()->session['autologin'])) {
				$cookie=Yii::app()->request->getCookies()->itemAt(Yii::app()->user->getStateKeyPrefix());
				if($cookie)
				{
				
					yii::app()->session['autologin'] = true;
					if(isset(Yii::app()->session['last_item_action']))
					{
						$this->redirect(Yii::app()->session['last_item_action']);
					}
					else
					{
						$this->redirect(array('/site/index'));  //
					}	
				}else{
				// timeout
				Yii::app()->user->logout();
				$this->redirect(array('/site/SessionTimeout'));  //
			}
			
			}
			else {
				$user_id = Yii::app()->user->getState('profile')->user_id;
				$reset_session = User::model()->findByPk($user_id)->reset_session;
				$module = Yii::app()->getController()->id;
				$action = Yii::app()->getController()->action->id;
				
				if($reset_session=='Y' && $action!='logout'){
					$this->redirect(array('/auth/logout'));
				}
				yii::app()->user->setState('userSessionTimeout', time() + Yii::app()->params['sessionTimeoutSeconds']) ;
				return true; 
			}
		} 
	    else 
	    {
	        return true;
	    }
        }*/
	
}
