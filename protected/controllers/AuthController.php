<?php
class AuthController extends Controller
{

	//public $layout='//layouts/layout-new';

	public function actionIndex()
	{
		$this->render('index');
	}

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	
	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		//$this->layout = $this->mobile.'login-layout';
		$service = Yii::app()->request->getQuery('service');

		
		// Check if the user is coming via the SSO Service (e.g. Google)
		if (isset($service)) {
	     		$authIdentity = Yii::app()->eauth->getIdentity($service);
	     		//$authIdentity->redirectUrl = Yii::app()->user->returnUrl;
				$authIdentity->redirectUrl = $this->createAbsoluteUrl('site/index');
	     		$authIdentity->cancelUrl = $this->createAbsoluteUrl('auth/login');
	      	
	     		if ($authIdentity->authenticate()) {
	      		$identity = new ServiceUserIdentity($authIdentity);
					//exit('here');
	      		// Successful login
	      		if ($identity->authenticate()) {
	      			
						Yii::app()->session['g_session'] = array(
							'lang' => $identity->getLanguage(),
							'fname' => $identity->getFirstName(),
							'lname' => $identity->getLastName(),
							'email' => $identity->getEmail() );
						/*echo '<pre>';
						print_r($identity);
						echo '</pre>';die;*/
		  		Yii::app()->user->login($identity);
		  		//var_dump(Yii::app()->user->isGuest);
		  		//var_dump(Yii::app()->user);
		  		
		  		//die();
		   	
		  		// Special redirect with closing popup window
		  		//$authIdentity->redirect('/site');
		  		$authIdentity->redirect();
	      		}
	      		else {
		  		// Close popup window and redirect to cancelUrl
		  		$authIdentity->cancel();
	      		}
	     		}
	     		// Something went wrong, redirect to login page
	     		$this->redirect(array('auth/login'));
 		}
		
		// Redirect to SSO URL if so requested, as normal users may also need to sign in
		if (yii::app()->user->getState('client_authtype') == 'sso' && CHttpRequest::getQuery('nosso') == '') {
			$this->ssoredir();
		}
		// User is signing in through the regular login
		
		$model=new LoginForm;
		
		// if it is ajax validation request
		//if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		if(Yii::app()->request->isAjaxRequest)
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if(Yii::app()->user->getState('logged_in')) {
		$this->redirect(array('site/index'));
		}
		$username = CHttpRequest::getQuery('username');
		$password = CHttpRequest::getQuery('password');
		if(isset($username) && isset($password) && $username!='' && $password!=''){
		$model->username = $username;
		$model->password = $password;
		if($model->validate() && $model->login())
		{
			yii::app()->user->setState('logged_in', true);
			if(isset(Yii::app()->session['last_item_action']))
			{
				$this->redirect(Yii::app()->session['last_item_action']);
			}
			$this->redirect(array('users/welcome'));
		}
		}
		
		// collect user input data
		if((Yii::app()->request->isPostRequest && Yii::app()->request->getPost('LoginForm')))
		{
			$model->attributes=Yii::app()->request->getPost('LoginForm');
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
			{
				yii::app()->user->setState('logged_in', true);
				if(isset(Yii::app()->session['last_item_action']))
				{
					$this->redirect(Yii::app()->session['last_item_action']);
				}
				$this->redirect(array('site/index'));
			}
		}
		
		
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	
	public function actionLogout()
	{
		// Added the concept related to sign in page redir in case it's not sso login - Vivek, 07 March 2013
		$logout_url = "";
		if (isset(Yii::app()->user->getState('profile')->is_sso) && Yii::app()->user->getState('profile')->is_sso == 'n') 
		{
			$logout_url = "&nosso=y";
		}
		Yii::app()->user->logout();
		$this->redirect(array("auth/login$logout_url"));
	}
	
	public function actionWidgetsPreference()
	{
		if(Yii::app()->request->isPostRequest)
		{
			foreach($_POST as $key=>$val)
			{
			//CHttpRequest::getPost('sort1');
			parse_str($val, $sort1);
			//print_r($sort1);
			foreach($sort1 as $key=>$value)
			{
				$widgets[] = implode(', ',$value);
			}
			}
			//print_r($widgets);exit();
			
			
			$widgetsPreference = serialize($widgets);
			
			$criteria=new CDbCriteria;
			$criteria->condition = "client_id ='".User::model()->findByPk(Yii::app()->user->id)->client_id."'  and user_id='".Yii::app()->user->id."'";
		$user = User::model()->findAll($criteria);
		foreach($user as $model)
		{
			$model->widgetpref = $widgetsPreference;
			if($model->save(false));
			{
				
			}
		}						
			//print_r(unserialize($s));

		}
		
	}

	// Toggle Mobile Theme Display
	public function actionMobile($url="") {

		$mobile = $this->mobile;
		
		if (yii::app()->user->getState('isMobileTheme') == "" || yii::app()->user->getState('isMobileTheme') == 'no') {
			yii::app()->user->setState('isMobileTheme','yes');
		} else {
			yii::app()->user->setState('isMobileTheme','no');
		}

		if ($url == '') { $url = yii::app()->createUrl('site/index'); }
		$this->redirect($url);
	}
	
	// Redirect the user to the client's SSO path. 06 March 2013 by Vivek
	// Create a client specific SSO redirection file which would be of the syntax - sso/clienttheme.php (e.g. /sso/actelion.php)
	public function ssoredir() {
		if(Yii::app()->request->isAjaxRequest) {
		return ;
		} else {
		header("Location: ./sso_link/".yii::app()->theme->name.".php");
		}
		exit;
	}
	
	// SSO Login function. Identifies if the Session is set, and then calls relevant functions
	// Added by Vivek Kapoor on 09 November 2012
	public function actionSsologin() {

			if (isset($_SESSION['SSO_EMAIL'])) {
				$email = $_SESSION['SSO_EMAIL'];
			} else {
				$email = '';
			}
			$this->layout = $this->mobile.'login-layout';
			
			$client_id = Yii::app()->user->getState('client_id');

			$model = new LoginForm;
			$model->user_email = $email;
			$model->is_sso = true;
			
			// Only if it is a mobile browser, then ask for stay signed in
			if (yii::app()->user->getState('isMobile') == 'yes') {
				if(!$_POST){
					// Added on 11 April 2013 by Mahak / Vivek regarding SSO Login Save functionality on Mobile
					$this->redirect(array('/auth/staysignedin'));				
					exit;
					}
				else{
					$post = $_POST['LoginForm'];
					$model->rememberMe = $post['rememberMe'];
				}
			}
			
			if ($model->login()) {
				unset($_SESSION['SSO_EMAIL']);

				// Check if anything in session, else redirect to index
                                if(isset(Yii::app()->session['last_item_action']))
                                {
                                        $this->redirect(Yii::app()->session['last_item_action']);
                                }
                                else
                                {
                                        $this->redirect(array('/site/index'));  //
                                }

			} else {
				// User is valid but is not able to sign in
				// Check the client's status first
				
				$model2 = new Clients;
				$criteria = new CDbCriteria;
				$criteria->select = array('id','auth_allow','auth_msg','auth_defaultcatg');
				$criteria->condition = "id='$client_id'";

				$record = $model2->find($criteria);

				if($record===null) {
					exit("Incorrect client information. Sign In cannot continue.");
				} else {
					$auth_allow = $record->auth_allow;
					$auth_msg = $record->auth_msg;
					$auth_defaultcatg = $record->auth_defaultcatg;
					
					// If Limited Access, then show appropriate message
					if ($auth_allow == 'limited') {

						$this->render('limited',array('auth_msg'=>$auth_msg));
						
					} else {
					
						// Add the user in database and make him sign in with default categories
						$model3 = new User;
						$model3->user_email = $email;
						$model3->user_fname = 'SSO 1';
						$model3->user_lname = 'SSO 2';
						$model3->date_added = new CDbExpression('NOW()');
						$model3->last_modified = new CDbExpression('NOW()');
						$model3->added_by = '1';
						$model3->category_access = $auth_defaultcatg;
						$model3->client_id = $client_id;
						$model3->status = 'a';
						$model3->access_level = '0';
						$model3->user_group = '0';
						$model3->user_name = rand();
						$model3->password = rand();
						$model3->timezone = 'Europe/Zurich';
						
						$model3->last_login = new CDbExpression('NOW()'); // added by Vir on 28 Jan 2013 required in statistics module
						
						if ($model3->save()) {
							// Once the user is added, redirect to same page so that a correct login can be done
							$this->redirect(array('auth/ssologin'));
						} else {
							exit("Unable to add user");
						}	
					}
				}
			}

	}

	public function actionStaysignedin() {
			
		$this->layout = $this->mobile.'login-layout';
		$model=new LoginForm;
		$this->render('staysignedin', array('model'=>$model));
	}
	
	public function actionForgotPassword()
	{
		if(Yii::app()->user->getState('logged_in')) {
		//exit;
		$this->redirect(array('site/index'));
		}
		$errorcode = array();
		$this->layout = $this->mobile.'login-layout';
		$email =  Yii::app()->request->getPost('username');
		$client_id = Yii::app()->user->getState('client_id');
		if(isset($email))
		{
			if($email=="")
			{
				Yii::app()->user->setFlash('error', "Please Enter the Email");
			}
			else{
			try
			{
				
				// 1. check if user exists or not for logged in client
				$userRecord = User::model()->find(array('condition'=>'user_email=:user_email and client_id=:client_id',
    'params'=>array(':user_email'=>$email, ':client_id'=>$client_id)));
				if ($userRecord) 
				{
					$resetpw = User::model()->resetPasswordMail($userRecord);
					if (!$resetpw) {
						
						Yii::app()->user->setFlash('error', "Error occured, Please try again.");
						$this->redirect(array("auth/".$this->mobile."forgotpassword"));
					}
					else
					{
						Yii::app()->user->setFlash('success', "Reset Password Instructions have been sent to your mail");
					}
				}
				else 
				{
					Yii::app()->user->setFlash('error', "Email not found, Please enter a valid Email!");
					$this->redirect(array("auth/".$this->mobile."forgotpassword"));
				}
				
				// All well, redirect to home page
				//$_SESSION[SESS_PREFIX.'success_code'] = array(2);
				$this->redirect(array("auth/".$this->mobile."login"));
				exit();
			}
			catch(Exception $e)
			{
				//$_SESSION[SESS_PREFIX.'error_code'] = $errorcode;
				$this->redirect(array("auth/".$this->mobile."forgotpassword?u=".urlencode(base64_encode($email))));
				exit();
			}
			}			
		}
		/*else
		{
			Yii::app()->user->setFlash('error', "Please Enter the Email");
			//$this->redirect(array("auth/".$this->mobile."forgotpassword"));
		}*/
		$this->render($this->mobile.'forgot_password');
	}
	
	public function actionResetPassword()
	{
		if(Yii::app()->user->getState('logged_in')) {
		//exit;
		$this->redirect(array('site/index'));
		}
		
		$user_model = new User();
		$this->layout = $this->mobile.'login-layout';
		$code = Yii::app()->request->getPost('code');
		if($code)
		{
			$verify = $user_model->resetPasswordVerify($code);
			if($verify)
			{
				$password = $_POST['password'];				
				$confirm_password = $_POST['confirm_password'];
				if(empty($password))
				{
					Yii::app()->user->setFlash('error', "Please enter password field!");
				}

				if($password != $confirm_password)
				{
					Yii::app()->user->setFlash('error', "Passwords do not match!");
				}	
				//$flashMessages = Yii::app()->user->getFlashes();
				if(!Yii::app()->user->hasFlash('error')){
					$reset = $user_model->resetPassword($password, $code);
			
					if (!$reset)
					{ 
						Yii::app()->user->setFlash('error', "Error occured, Please try again.");
					}
					else
					{
						Yii::app()->user->setFlash('success', "Password Changed!");
						$this->redirect(array("auth/login"));	
						exit;						
					}
				}
				else{
					$this->redirect(array("auth/resetpassword&c=".$code));
					exit;
				}
			}
			else
			{
				Yii::app()->user->setFlash('error', "Invalid Url!");
			}
		}
		$code = Yii::app()->request->getQuery('c');
		
		if(!$code){
			Yii::app()->user->setFlash('error', "Invalid Url!");
		}
		
		$this->render('resetpassword', array('code'=>$code));
	}

	// 404 Error - Custom
	public function actionError404() {
		$heading = 'Error 404';
		//$this->layout = $this->mobile.'main';
		$this->layout = '/layout/blank';
		
		$this->render('404');
	
	}
}
