<?php

class UsersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	//public $layout='//layouts/layout';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'register'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'welcome'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Users;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Users']))
		{
			$model->attributes=$_POST['Users'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Users');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Users('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Users']))
			$model->attributes=$_GET['Users'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='users-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionRegister()
	{
		$user_model=new Users('register');
		$creator_model=new CreatorDetails('register');
		$invite_id = CHttpRequest::getQuery('invite_id');
		if($invite_id!=""){
			$find_invite_id = Randomlookups::model()->find("random_num='$invite_id'");
			//echo $find_invite_id
			//exit;
			if($find_invite_id)
			{
				//$user_model->name = 
				$user_model->circle_beneficiary = $find_invite_id->user_id;
				//$invite_sender_details = Users::model()->find("id='$user_id'");
				//$user_model->circle_beneficiary = 
			}
			else{
				throw new CHttpException(400,"The url is invalid. Please contact <a href='mailto:team@helixeffort.com'>helixeffort Team</a>");
			}
		}
		$transaction = Yii::app()->db->beginTransaction();
		try
		{	
			if(isset($_POST['Users']))
			{
				$user_model->attributes=$_POST['Users'];
				$user_model->date_added = new CDbExpression('NOW()');
				$user_model->last_modified = '0000-00-00 00:00:00';
				$user_model->status = 'a';
				$password = $user_model->password;
				$user_model->password = new CDbExpression("password('".$user_model->password."')"); // Need to sanitize

				if($user_model->validate())
				{
					$user_model->save();
					$id = $user_model->id;
				}
			}

		if(isset($_POST['CreatorDetails'])&& isset($id))
		{
			$creator_model->attributes=$_POST['CreatorDetails'];
			$creator_model->user_id = $user_model->id;
			if($creator_model->validate())
			{
				$creator_model->save();
			}
			$transaction->commit();
			$this->redirect(array('auth/login','username'=>$user_model->username, 'password'=>$password));
			//$this->render('welcome',array('user_model'=>$user_model));
			//$this->redirect(array('view','id'=>$user_model->id));
		}
	}
	catch(Exception $e)
        {
        	$transaction->rollback();
        }
	$this->render('register',array('user_model'=>$user_model, 'creator_model'=>$creator_model));
	}

	public function actionWelcome()
	{
	$this->render('welcome');
	}
}
