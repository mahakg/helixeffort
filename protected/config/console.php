<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Console Application',
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.components.smtp.*',
	),
	// application components
	'components'=>array(
		/*'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),*/
		// uncomment the following to use a MySQL database
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=helixeffort',
			//'tablePrefix' => 'ip_',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'abc123',
			'charset' => 'utf8',
		),
		'servers'=>array(
		        array('host'=>'localhost', 'port'=>11211, 'weight'=>60),
		        array('host'=>'server2', 'port'=>11211, 'weight'=>40),
		),	
	),
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'attachment'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../', // Path for attachemnt
                'earningscall'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../files/attachments/earningscall', // Path for Earnings Call
                'newsfiles'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../files/attachments/news', // Path for news
                'articlesfiles'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../files/attachments/articles', // Path for Articles
                'profilepic'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'../../../files/profilepic', //Path for Profile Pic
	),
);
