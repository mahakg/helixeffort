<?php

class MailQueueCommand extends CConsoleCommand
{
	public function run()
    	{
		$mail = new PHPMailer;
		
		$mail->IsSMTP();
		$mail->Host  = "localhost";
		$mail->SMTPAuth = "";

		// Get the emails from database
		
		$criteria = new CDbCriteria;
		
		$criteria->condition = "error='n'";
		//$criteria->select = ('SQL_CALC_FOUND_ROWS *'); 
		//$criteria->with = 'ipContentRepositories';
		//$criteria->alias = 'c';
		$criteria->offset = 0;
		$criteria->limit = 30;
		//$criteria->order = 'date_modified desc';
		
		$queue = Queue::model()->findAll($criteria);

		foreach($queue as $key=>$row) {
		//print_r($row);
		$mail->ClearAddresses();
                $mail->ClearCCs();
                $mail->ClearBCCs();
		$mail->IsHTML(true);
                $mail->CharSet = 'UTF-8';
		$mail->SetFrom($row->senderemail,$row->sendername);
		$mail->Subject    = $row->subject;
		$mail->AddAddress($row->recpemail, "");
		$mail->AltBody = wordwrap(strip_tags($row->message));
		$mail->MsgHTML(wordwrap($row->message));

		// Get attachments if any
		if ($row->attachments != "") {
			$attacharray = explode("%%_n_%%",$row->attachments);
			for ($i=0;$i<sizeof($attacharray);$i++) {
			$mail->AddAttachment("$attacharray[$i]",basename($attacharray[$i]));
			}
		}
		
		
		if($row->cc !=  '')
                {
                        $cc_addrs = explode(',', $row->cc);
                        foreach($cc_addrs as $cc_addr)
                        {
                                $mail->AddCC($cc_addr, "");
                        }
                }
                if($row->bcc !=  '')
                {
                        $bcc_addrs = explode(',', $row->bcc);
                        foreach($bcc_addrs as $bcc_addr)
                        {
                                $mail->AddBCC($bcc_addr, "");
                        }
                }

		
		
		if(!$mail->Send()) {
		  echo "Mailer Error: " . $mail->ErrorInfo;
		  $update_queue = Queue::model()->updateByPk($row->id, array('error'=>'Y'));
		}
		else {
			// delete from database
			$del_queue = Queue::model()->deleteByPk($row->id);
		}
}


echo "Process completed at " .date("Y-m-d H:i:s")."\n";
sleep(5);
		
    	}
}
?>
