<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Create Users','url'=>array('create')),
	array('label'=>'Update Users','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete Users','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users','url'=>array('admin')),
);
?>
<div id="templatemo_main_innerpages" style="margin-bottom: 30px;" > <span class="top"></span><span class="bottom"></span>
<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'association',
		'email',
		'password',
		'username',
		'circle_beneficiary',
		'date_added',
		'last_modified',
		'status',
		'signin_expiry',
		'timezone',
		'about',
		'profile_image',
		'last_login',
		'user_type',
		'user_country',
		'reset_session',
	),
)); ?>
</div>
