<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Users','url'=>array('index')),
	array('label'=>'Manage Users','url'=>array('admin')),
);
?>
<div class="hero-unit" style="margin-top: 30px;">
<h1>Create Profile</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

</div>
