<div class="form">
<div class="hero-unit" style="margin-top: 30px;" >
<?php $form=$this->beginWidget('bootstrap.widgets.BootActiveForm',array(
	'id'=>'horizontalForm',
    'type'=>'horizontal',
	'enableAjaxValidation'=>false,
)); ?>

<div style="width:80%">
<h2>Creator Registration Page</h2>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
	
	
	<div class="controls" style='padding:5px;'><h4>Representer Details</h4></div>
	
	<?php //echo $form->errorSummary($user_model); ?>

	<?php //echo $form->errorSummary($creator_model); ?>
	<?php echo $form->textFieldRow($user_model,'name',array('class'=>'span5','maxlength'=>50, 'hint'=>'Every creator profile has to be represented by an individual who can be accountable for the interactions within the circle. The full name of the representative or an alias or a pen name familiarly known to the public can be provided.')); ?>

	<?php $association_array = array(1=>'Maker/Creator/Founder/Partner',2=>'Manager',3=>'Producer/Investor',4=>'Actor',5=>'other'); ?>
	
    	<?php echo $form->dropDownListRow($user_model, 'association', $association_array,array('hint'=>'Relationship with the creator name')); ?>

	<?php echo $form->textFieldRow($user_model,'email',array('class'=>'span5','maxlength'=>50, 'hint'=>'The email address will be used for all transactions with respect to payments and audience interactions. the email address will be used for authenticating your creator status with your item')); ?>

	<?php echo $form->passwordFieldRow($user_model,'password',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textFieldRow($user_model,'username',array('class'=>'span5','maxlength'=>45, 'hint'=>'Can be used for creator login/User login')); ?>
	<? if($user_model->circle_beneficiary==""){ ?>
	<div style="background:#eee999; padding: 5px; border-radius: 7px;">
	<?php echo $form->textFieldRow($user_model,'circle_beneficiary',array('class'=>'span5','maxlength'=>50, 'hint'=>'Tag a circle-beneficiary with whom you associate with. Circle-beneficiaries are creators present within the circle and will bring you within the circle', 'append'=>'<div id=note_div class=help-inline style="white-space: pre-wrap;white-space: -moz-pre-wrap;white-space: -pre-wrap;white-space: -o-pre-wrap;  word-wrap: break-word;">Note: Circle Beneficiary cannot process-request if represented by the same person(deaf-circle). To avail same representation ID kindly approach a different Circle-beneficiary not represented by you.</div>')); ?>
	</div>
	<? }else{ 
	echo $form->hiddenField($user_model,'circle_beneficiary',array());
	}
	?>
	<div class="controls"><h4>Creator Details</h4></div>
	
	<?php echo $form->textFieldRow($creator_model,'identity_name',array('class'=>'span5','maxlength'=>50, 'hint'=>'This can be the name under which the public consumes your content. It could be an organisation, company, artist, band, author, director, etc.')); ?>
	
	<?php echo $form->textFieldRow($creator_model,'place_of_origin',array('class'=>'span5','maxlength'=>50, 'hint'=>'There could be multiple creators with the same name, so a place of origin could help the audience distinguish from the ones they want to interact with')); ?>
	
	<?php echo $form->textFieldRow($creator_model,'license_type',array('class'=>'span5','maxlength'=>50, 'hint'=>'Every Hyperproduct created for your items will depend upon the kind of license operating upon. This will be your default license as how it is bwing consumed by your audience. You can change a license individually for each item under item profiles.')); ?>
	
	<?php echo $form->textFieldRow($creator_model,'copyright',array('class'=>'span5','maxlength'=>50, 'hint'=>'Full title')); ?>
	
	<?php echo $form->textFieldRow($creator_model,'tag_similar_items',array('class'=>'span5','maxlength'=>50, 'hint'=>'Name of the Creator or the Item')); ?>
	
	<? /*	<?php echo $form->labelEx($creator_model,'user_id'); ?>
		<?php echo $form->textField($creator_model,'user_id'); ?>
		<?php echo $form->error($creator_model,'user_id'); ?>

		<?php echo $form->labelEx($creator_model,'identity_name'); ?>
		<?php echo $form->textField($creator_model,'identity_name'); ?>
		<?php echo $form->error($creator_model,'identity_name'); ?>

		<?php echo $form->labelEx($creator_model,'place_of_origin'); ?>
		<?php echo $form->textField($creator_model,'place_of_origin'); ?>
		<?php echo $form->error($creator_model,'place_of_origin'); ?>

		<?php echo $form->labelEx($creator_model,'license_type'); ?>
		<?php echo $form->textField($creator_model,'license_type'); ?>
		<?php echo $form->error($creator_model,'license_type'); ?>

		<?php echo $form->labelEx($creator_model,'copyright'); ?>
		<?php echo $form->textField($creator_model,'copyright'); ?>
		<?php echo $form->error($creator_model,'copyright'); ?>

		<?php echo $form->labelEx($creator_model,'tag_similar_items'); ?>
		<?php echo $form->textField($creator_model,'tag_similar_items'); ?>
		<?php echo $form->error($creator_model,'tag_similar_items'); ?>

<? /*	<?php echo $form->textFieldRow($user_model,'date_added',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($user_model,'last_modified',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($user_model,'status',array('class'=>'span5','maxlength'=>1)); ?>

	<?php echo $form->textFieldRow($user_model,'signin_expiry',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($user_model,'timezone',array('class'=>'span5','maxlength'=>30)); ?>

	<?php echo $form->textAreaRow($user_model,'about',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($user_model,'profile_image',array('class'=>'span5','maxlength'=>50, 'hint'=>''); ?>

	<?php echo $form->textFieldRow($user_model,'last_login',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($user_model,'user_type',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($user_model,'user_country',array('class'=>'span5','maxlength'=>250)); ?>

	<?php echo $form->textFieldRow($user_model,'reset_session',array('class'=>'span5','maxlength'=>1)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.BootButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$user_model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>
<? /*	<div class="row">
		<?php echo $form->labelEx($user_model,'date_added'); ?>
		<?php echo $form->textField($user_model,'date_added'); ?>
		<?php echo $form->error($user_model,'date_added'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'last_modified'); ?>
		<?php echo $form->textField($user_model,'last_modified'); ?>
		<?php echo $form->error($user_model,'last_modified'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'status'); ?>
		<?php echo $form->textField($user_model,'status'); ?>
		<?php echo $form->error($user_model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'profile_image'); ?>
		<?php echo $form->textField($user_model,'profile_image'); ?>
		<?php echo $form->error($user_model,'profile_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'timezone'); ?>
		<?php echo $form->textField($user_model,'timezone'); ?>
		<?php echo $form->error($user_model,'timezone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'reset_session'); ?>
		<?php echo $form->textField($user_model,'reset_session'); ?>
		<?php echo $form->error($user_model,'reset_session'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'user_type'); ?>
		<?php echo $form->textField($user_model,'user_type'); ?>
		<?php echo $form->error($user_model,'user_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'user_country'); ?>
		<?php echo $form->textField($user_model,'user_country'); ?>
		<?php echo $form->error($user_model,'user_country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'signin_expiry'); ?>
		<?php echo $form->textField($user_model,'signin_expiry'); ?>
		<?php echo $form->error($user_model,'signin_expiry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'about'); ?>
		<?php echo $form->textField($user_model,'about'); ?>
		<?php echo $form->error($user_model,'about'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($user_model,'last_login'); ?>
		<?php echo $form->textField($user_model,'last_login'); ?>
		<?php echo $form->error($user_model,'last_login'); ?>
	</div>
*/
?>
	<div class="controls">
    <?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Create My Profile')); ?>
    <?php //$this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'reset', 'label'=>'Reset')); ?>
</div>
</div>
<?php $this->endWidget(); ?>
</div>
</div><!-- form -->
<script>
$('#Users_association').tooltip({'trigger':'focus', 'title': 'Association/Relation with Creator Identity Name'});

$('#Users_circe_beneficiary').tooltip({'trigger':'focus', 'title': 'Tag a circle beneficiary with whom you associate with'});
$('#CreatorDetails_identity_name').tooltip({'trigger':'focus', 'title': 'This can be name under which public consumes your content. This could be name of an Organisation, Creator, Artist'});
$('#CreatorDetails_place_of_origin').tooltip({'trigger':'focus', 'title': 'There can be multiple creators with same name. So place of origin can help the audience to distinguish the creator they want to interact with'});

$('#CreatorDetails_license_type').tooltip({'trigger':'focus', 'title': 'Type of license on your identity, if any'});
$('#CreatorDetails_copyright').tooltip({'trigger':'focus', 'title': 'copyright, if any'});
$('#CreatorDetails_tag_similar_items').tooltip({'trigger':'focus', 'title': 'Tag Similar items. Tags might help you to give users a suggestion who like your tagged items.'});
</script>
