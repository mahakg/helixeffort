<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="hero-unit" style="margin-top: 30px;" >
<h2 align=center>Login</h2>


<div class="form" style='margin: 0 auto; width: 30%;'>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', array('class'=>'span5')); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php $model->password = ''; ?>
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password' , array('class'=>'span5')); ?>
		<?php echo $form->error($model,'password'); ?>
	<!--	<p class="hint">
			Hint: You may login with <tt>demo/demo</tt> or <tt>admin/admin</tt>.
		</p>-->
	</div>

<!--	<div class="row rememberMe">
		<?php echo $form->checkBox($model,'rememberMe'); ?>
		<?php echo $form->label($model,'rememberMe'); ?>
		<?php echo $form->error($model,'rememberMe'); ?>
	</div>-->

	<div class="row buttons">
    <?php $this->widget('bootstrap.widgets.BootButton', array('buttonType'=>'submit', 'type'=>'primary', 'label'=>'Login')); ?>
	</div>

<?php $this->endWidget(); ?>
</div><!-- form -->
</div>
