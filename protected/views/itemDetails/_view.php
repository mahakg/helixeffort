<div class="display_items">
	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::encode(CHtml::encode($data->id)); ?>
	<br />
<?
/*	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />
*/ ?>
	<b><?php echo CHtml::encode($data->getAttributeLabel('item_name')); ?>:</b>
	<?php echo CHtml::encode($data->item_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_desc')); ?>:</b>
	<?php echo CHtml::encode($data->item_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('release_date')); ?>:</b>
	<?php echo CHtml::encode($data->release_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_category')); ?>:</b>
	<?php echo CHtml::encode(ItemCategories::model()->findByPk($data->item_category)->category_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('audience_drive')); ?>:</b>
	<?php echo CHtml::encode($data->audience_drive); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('licence_type')); ?>:</b>
	<?php echo CHtml::encode($data->licence_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('copyright')); ?>:</b>
	<?php echo CHtml::encode($data->copyright); ?>
	<br />

	*/ ?>
	
	<a href="<?=$this->createUrl('itemDetails/update'). '&id='.$data->id; ?>" class="btn"><i class="icon-edit"></i> <strong>Edit</strong></a>
<a href="#" class="btn"><i class="icon-trash"></i> <strong>Delete</strong></a>

</div>

