<?php $form=$this->beginWidget('bootstrap.widgets.BootActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<?php echo $form->textFieldRow($model,'id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'user_id',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'item_name',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'item_desc',array('class'=>'span5','maxlength'=>2048)); ?>

	<?php echo $form->textFieldRow($model,'release_date',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'item_category',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'audience_drive',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'licence_type',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'copyright',array('class'=>'span5','maxlength'=>200)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.BootButton', array(
			'type'=>'primary',
			'label'=>'Search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
