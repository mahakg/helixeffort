<?php
$this->breadcrumbs=array(
	'Item Details'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ItemDetails','url'=>array('index')),
	array('label'=>'Manage ItemDetails','url'=>array('admin')),
);
?>
<div class="hero-unit" style="margin-top: 30px;" > <span class="top"></span><span class="bottom"></span>
<h2>Add New Item to your Profile</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
</div>
