<?php
$this->breadcrumbs=array(
	'Item Details'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ItemDetails','url'=>array('index')),
	array('label'=>'Create ItemDetails','url'=>array('create')),
	array('label'=>'View ItemDetails','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage ItemDetails','url'=>array('admin')),
);
?>
<div class="hero-unit" style="margin-top: 30px;">
<h1>Update Item <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
</div>
