<?php
$this->breadcrumbs=array(
	'Item Details'=>array('index'),
	$model->id,
);
?>
<div class="hero-unit">
<?
$this->menu=array(
	array('label'=>'List ItemDetails','url'=>array('index')),
	array('label'=>'Create ItemDetails','url'=>array('create')),
	array('label'=>'Update ItemDetails','url'=>array('update','id'=>$model->id)),
	array('label'=>'Delete ItemDetails','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ItemDetails','url'=>array('admin')),
);
?>

<h2>View ItemDetails #<?php echo $model->id; ?></h2>

<?php $this->widget('bootstrap.widgets.BootDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
//		'user_id',
		'item_name',
		'item_desc',
		'release_date',
		'item_category'=>array('name'=>'item_category', 'value'=>ItemCategories::model()->findByPk($model->item_category)->category_name),
		'audience_drive',
		'licence_type',
		'copyright',
	),
)); ?>
</div>
