<?php $form=$this->beginWidget('bootstrap.widgets.BootActiveForm',array(
	'id'=>'item-details-form',
	'enableAjaxValidation'=>false,
)); ?>
<div style="width:70%">
	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
	<? $profile = Yii::app()->user->getState('profile'); ?>
	<?	$user_id = $profile->id; ?>
	<?php echo $form->hiddenField($model,'user_id',array('class'=>'span5', 'value'=>$user_id)); ?>

	<?php echo $form->textFieldRow($model,'item_name',array('class'=>'span5','maxlength'=>100,'hint'=>'Please use the right capitalizations, speling and grammar as how the item is being consumed by the users. The item name will create an Item profile which will be listed within the creator profile')); ?>

	<?php echo $form->textFieldRow($model,'item_desc',array('class'=>'span5','maxlength'=>2048,'hint'=>'A small description about the item')); ?>

	<?php echo $form->textFieldRow($model,'release_date',array('class'=>'span5')); ?>
	<?php echo $form->dropDownListRow($model,'item_category', CHtml::listData(ItemCategories::model()->findAll(array('order' => 'id')),'id','category_name'));
//	echo $form->textFieldRow($model,'item_category',array('class'=>'span5')); ?>

	<?php echo $form->textFieldRow($model,'audience_drive',array('class'=>'span5','maxlength'=>100)); ?>

	<?php echo $form->textFieldRow($model,'licence_type',array('class'=>'span5','maxlength'=>200)); ?>

	<?php echo $form->textFieldRow($model,'copyright',array('class'=>'span5','maxlength'=>200)); ?>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.BootButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>
</div>
<?php $this->endWidget(); ?>
<script>
$(function() {
$( "#ItemDetails_release_date" ).datepicker({dateFormat:'yy-mm-dd'});
});
</script>
