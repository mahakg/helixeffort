<?php
$this->breadcrumbs=array(
	'Item Details',
);

$this->menu=array(
	array('label'=>'Create ItemDetails','url'=>array('create')),
	array('label'=>'Manage ItemDetails','url'=>array('admin')),
);
?>
<div class="hero-unit">
<h2>Items Added By You</h2>
<?php $this->widget('bootstrap.widgets.BootListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'id'=>1,
)); ?>
</div>
