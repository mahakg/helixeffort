﻿<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>helixeffort - consume.experience.hyperproduce.</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS -->
<link href="<?php echo $this->assetsBase ?>/css/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->assetsBase ?>/css/showcase.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->assetsBase ?>/css/bootstrap-yii.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $this->assetsBase ?>/css/custom.css" type="text/css" media="screen" />

<?php $cs=Yii::app()->clientScript;
$cs->scriptMap=array(
        'jquery.js'=>false,
        'bootstrap.min.js'=>false,
        'bootstrap.min.css'=>false,
        'bootstrap-yii.css'=>false,
); ?>
<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery-ui.js"></script>
<link href="<?php echo $this->assetsBase ?>/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery.yiiactiveform.js"></script>

    <style type="text/css">

      /* Sticky footer styles
      -------------------------------------------------- */

      html,
      body {
        height: 100%;
       	background: #f5f5f5;
        /* The html and body elements cannot have any padding or margin. */
      }

      /* Wrapper for page content to push down footer */
      #wrap {
        min-height: 100%;
        height: auto !important;
        height: 100%;
        /* Negative indent footer by it's height */
        margin: 0 auto -60px;
      }

      /* Set the fixed height of the footer here */
      #push,
      #footer {
        height: 60px;
      }
      #footer {
        background-color: black;
      }

      /* Lastly, apply responsive CSS fixes as necessary */
      @media (max-width: 767px) {
        #footer {
          margin-left: -20px;
          margin-right: -20px;
          padding-left: 20px;
          padding-right: 20px;
        }
      }
      .right-menu{
	width:20%;
	float: right; 
	margin-top: 10px; 
	margin-bottom: 10px;
      }

	.col_l { margin: 0 }

      /* Custom page CSS
      -------------------------------------------------- */
      /* Not required for template or sticky footer method. */

      #wrap > .container {
        padding-top: 60px;
      }
      .container .credit {
        margin: 20px 0;
      }

      code {
        font-size: 80%;
      }
      
      #templatemo_main_wrapper {
	clear: both;
	position: relative;
	margin:auto;
	margin-bottom: 30px; 
	margin-top:30px; 
	background: white;
	border-radius: 5px 5px 5px 5px;
    	padding: 10px;
}
#templatemo_main {
	margin: 0 auto;
    padding: 15px;
    width: 80%;
    z-index: 0;
}

#templatemo_main table tr:nth-child(odd) {
    background-color: #e4e4e4;
}
#templatemo_main table tr:nth-child(even) {
    background-color: #ffffff;
}
#templatemo_main table tr:hover { background: #bbe5e5; } 

a.headerlinks{
      color: #000;
      &:hover {
         color: #000;
       }
     }

.col_2 { float: left; width: 430px; padding-right: 30px } 

.rp_pp { margin-bottom: 10px; padding-bottom: 10px; border-bottom: 1px dotted #ced4dc }
.rp_pp img { float: left; margin-right: 15px; border: 3px solid #fff }
.rp_pp p { font-size: 10px }
input.span5{
height: 30px;
}
#templatemo_footer {
	width: 960px;
	margin: 0 auto;
	padding: 20px 10px 0;
	color: #725e91;
}

.col_4 { float: left; width: 215px; margin-right: 20px }

p.help-block {
font-size:12px;
width: 50%;
}
.help-block {
margin-top:2px;
}
.hero-unit p{
line-height:15px;
}
.help-inline{
color:red;
font-size:11px;
}
.add-on{
max-width:280px;
}
#note_div .{

}
.display_items{
font-size: 14px;
background-color: #efefef;
border-radius: 6px 6px 6px 6px;
font-weight: 200;
line-height: 20px;
margin-bottom: 30px;
margin-top: 30px;
padding: 10px;
}

.nav pull-right{
float:right important;
}
a.brand{
padding: 0 !important;
}
form.form-search{
margin:0;
}
</style>

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->

    <!-- Fav and touch icons -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
      <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
                                   <link rel="shortcut icon" href="../assets/ico/favicon.png">
  </head>

  <body>

<?
$user = 'Guest';
$logged_in = Yii::app()->user->getState('logged_in');
if($logged_in){
$profile = Yii::app()->user->getState('profile');
$user = $profile->name;
}
?>


    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">
      <div class="navbar navbar-fixed-top navbar-inverse">
	<div class="navbar-inner">
		
		<div class="container-fluid">
						<a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</a>
						
						<div aria-label="Main menu" role="navigation" class="nav-collapse collapse">
				
<ul style1="visibility:hidden;" role="menubar" class="nav">
<li class="item-101 current active"><a class="brand" href="index.php"><img src="<?php echo $this->assetsBase ?>/images/logo-new.jpg" alt="The helixeffort Project"></a></li>
<!--<li class="item-108 dropdown parent" aria-haspopup="true"><a href="/helix01/index.php?r=site/concept">Concept</a></li>
<li class="item-114 dropdown parent" aria-haspopup="true"><a href="/helix01/index.php?r=site/howitworks">How it Works</a></li>

<!--<li class="item-115 dropdown parent" aria-haspopup="true"><a href="#">Home</a></li>-->

<li aria-haspopup="true" class="item-115 dropdown parent"><a data-toggle="dropdown" class="dropdown-toggle" href="#">More<b class="caret"></b></a><ul aria-hidden="true" role="menu" class="dropdown-menu">
<li class="item-128" role="menuitem" tabindex="-1"><a href="<?php echo $this->createUrl('site/thehelixeffortproject') . '&id=1'; ?>">the helixeffort project</a></li>
<li class="item-129" role="menuitem" tabindex="-1"><a href="<?php echo $this->createUrl('site/feedback'); ?>"> Feedback </a> </li>
<li class="item-129" role="menuitem" tabindex="-1"><a href="<?php echo $this->createUrl('site/faq'); ?>"> FAQs </a> </li> 
<li class="item-129" role="menuitem" tabindex="-1"><a href="<?php echo $this->createUrl('site/contact'); ?>">  Contact</a> </li></ul></li></ul>

<ul class="nav pull-right" style="margin:0;padding:0"><li style="margin:0;padding:0"><div class="search visible-desktop" style="margin:0;padding:0">
				
<form action="/index.php" method="post" class="form-search">
	<div class="search-module">
		<input name="searchword" id="mod-search-searchword" maxlength="20" size="20" value="Search..." onblur="if (this.value=='') this.value='Search...';" onfocus="if (this.value=='Search...') this.value='';" class="span5" type="text">		<input name="task" value="search" type="hidden">
		<input name="option" value="com_search" type="hidden">
		<input name="Itemid" value="101" type="hidden">
	</div>
</form>

			</div></li></ul><ul class="nav pull-right" role="menubar">
    <li class="divider-vertical"></li>
    <li class="dropdown">
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <i class="icon-user icon-white"></i>&nbsp;&nbsp;Welcome, Guest <b class="caret"></b>
        </a>
        
             <? if(!$logged_in){ ?>
        <div id="login-dropdown" class="dropdown-menu">
            <form id="form-login" method="post" action="<?=$this->createUrl('auth/login'); ?>">
                <div class="control-group">
                    <label for="prependedInput" class="control-label"></label>

                    <div class="controls">
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-user"></i></span>
                            <input class="span2" name="LoginForm[username]" id="LoginForm_username" type="text">
                        </div>
                        <div class="input-prepend">
                            <span class="add-on"><i class="icon-asterisk"></i></span>
                            <input class="span2" name="LoginForm[password]" id="LoginForm_password" value="" type="password">
                        </div>    
                                                        <div class="help-block login-remember">
                                <label class="checkbox">
                                    <input value="yes" name="remember" type="checkbox"> Remember Me                                </label>
                                <input value="Log in" class="btn" name="Submit" type="submit">
                            </div>
                                                        <input value="com_users" name="option" type="hidden">
                            <input value="user.login" name="task" type="hidden">
                            <input value="aW5kZXgucGhwP29wdGlvbj1jb21fY29udGVudCZ2aWV3PWZlYXR1cmVkJkl0ZW1pZD0xMDE=" name="return" type="hidden">
                            <input value="1" name="71005b933f2a105f4520de77ed4690c8" type="hidden">                        </div>
                  		<a href="/index.php?option=com_users&amp;view=reset">Forgot your password?</a>
						<a href="/index.php?option=com_users&amp;view=remind">Forgot your username?</a>
                    </div>
            </form>
        </div>
 		<? }else{ ?>
				<ul class="dropdown-menu">
				<li><a href="/user/preferences"><i class="icon-cog"></i> Preferences</a></li>
				<li><a href="/help/support"><i class="icon-envelope"></i> Contact Support</a></li>
				<li class="divider"></li>
				<li><a href="<?=$this->createUrl('auth/logout'); ?>"><i class="icon-off"></i> Logout</a></li>
				</ul>
            <? } ?>
</li></ul>

			</div>
					</div>
	
	
	</div>

</div>
	<div style="clear:both;"></div>
<div class="custom" style="background: #999999;"> <div style="float:left; width:70%; padding:10px 10px;"><p><a href="index.php"><img alt="joostrap-logo-demo" src="<?php echo $this->assetsBase ?>/images/he-logo1.jpeg"></a></p>
</div>
<div  class="right-menu">
						<div align="center" style="font-size:15px;">Universal Worth</div>
						<div align="center" style="clear:both; font-size:35px; padding: 10px 10px;">1.05</div>
						<? if(!$logged_in){ ?>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('users/register'); ?>" style="font-size:15px;" class="headerlinks">Register as a Creator/User</a></div>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('auth/login'); ?>" style="font-size:15px;" class="headerlinks">Sign in</a></div>
						<? }else{ ?>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('itemDetails/create'); ?>" style="font-size:15px;" class="headerlinks">Add New Item</a></div>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('itemDetails/index'); ?>" style="font-size:15px;" class="headerlinks">List Existing Items</a></div>
						<? } ?>
						</div>
	<div style="clear:both;"></div>
 </div>


      <!-- Begin page content -->
     <!-- <div class="container">
        <div align="center" class="search-container">
		    <div class="page-header">
		      <h2 style="margin:auto; padding: 10px;"><span class="mega-icon mega-icon-search-input"></span> Search more than <strong>11.8M</strong> Items</h2>

		      <form accept-charset="UTF-8" action="/search" class="search_repos" id="search_form" method="get">
			<select>
			<option value="">Creator/Artist</option>
			<option value="">Audio</option>
			<option value="">Audio</option>
			<option value="">Video</option>
			<option value="">Book</option>
			</select><input type="text" class="search-page-input js-search-query" name="q" value="" tabindex="2" autocapitalize="off" autofocus="">
		<input type="hidden" id="type_value" name="type" value="">
		<input type="submit" value="Search" tabindex="3">

			<input type="hidden" name="ref" value="simplesearch">
		</form>      <div class="clear"></div>
		    </div>
		  </div>
      </div>
      -->

<?php echo $content; ?>
	
   
    <div id="templatemo_footer">
    	<div class="col_4">
        	<h5 align="center">Quick Links:</h5>
            <ul class="footer_list">
            	<li><a href="#">FAQs</a></li>
                <li><a href="#">Theory</a></li>
                <li><a href="">Community</a></li>
                <li><a href="blog.html">Privacy Policy</a></li>
                <li><a href="contact.html">Disclaimers</a></li>
			</ul>
        </div>
        <div class="col_4">
        	<h5 align="center">&nbsp;</h5>
            <ul class="footer_list">
                <li><a href="#">Site Map</a></li>
            	<li><a href="#">helixeffort.org</a></li>
                <li><a href="#">Contact Us</a></li>
               <!-- <li><a href="#"></a></li>
                <li><a href="#"></a></li>-->
		</ul>             
        </div>
        <div class="col_4">
        	<h5 align="center">Twitter</h5>
            <ul class="twitter_post">
	            <li>Suspendisse at scelerisque urna. Aenean tincidunt massa in tellus varius. <a href="#">http://bit.ly/13IwZO</a></li>
                <li>Proin dignissim, diam nec enim lorem tempus orci, ac ante purus in justo scelerisque. <a href="#">http://bit.ly/34sdPo</a></li>
			</ul>
        </div>
        <div class="col_4 col_l">
        	<h5 align="center">Follow Us</h5>
            <p><a href="#" target="_parent">Lorem ipsum</a> Suspendisse at scelerisque urna. Aenean tincidunt massa in tellus varius. <a href="#">In scelerisque</a> egestas est non convallis at nisi.</p>
            <div class="footer_social_button">
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/facebook-32x32.png" title="facebook" alt="facebook" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/flickr-32x32.png" title="flickr" alt="flickr" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/twitter-32x32.png" title="twitter" alt="twitter" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/youtube-32x32.png" title="youtube" alt="youtube" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/rss-32x32.png" title="rss" alt="rss" /></a>
			</div>
		</div>
        <div style="clear:both;"></div>
</div>

<div id="footer">
      <div class="container">
        <p class="muted credit">
© 2013 All rights reserved. helixeffort.com
</p>
      </div>
    </div>

 <script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery-easing-min.js"></script>
		<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery.ExpandyShowcase.js"></script>
		<script type="text/javascript">
			$(window).load(function() {
				$('#expandy').expandy({
				'animationDuration' : 250,
				'slideEasing' : 'easeOutBounce',
				'textSizeEasing' : 'easeInOutExpo',
				'fontSizeSmall' : '14px',
				'fontSizeLarge' : '32px',
				'compressedSize' : '100px',
				'expandedSize' : '500px',
				'firstOpen' : 0
				});
			}); 

		</script> 
		
<script>
// Google Analytics Tracking
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-41212760-1', 'helixeffort.com');
  ga('send', 'pageview');
</script>
  </body>
</html>

