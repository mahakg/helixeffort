<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Helixeffort - Consume, Appreciate, Hyperproduce</title>
<link href="<?php echo $this->assetsBase ?>/css/templatemo_style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->assetsBase ?>/css/showcase.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $this->assetsBase ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<script src="http://bootsnipp.com/js/jquery.js"></script>
	<script src="http://bootsnipp.com/js/bootstrap.min.js"></script>

<? Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCoreScript('jquery.ui');
Yii::app()->clientScript->registerCssFile(
Yii::app()->clientScript->getCoreScriptUrl().'/jui/css/base/jquery-ui.css');

 ?>
<!--<script type="text/javascript" src="js/ddsmoothmenu.js">
</script>-->

<script type="text/javascript">

/*ddsmoothmenu.init({
	mainmenuid: "templatemo_menu", //menu DIV id
	orientation: 'h', //Horizontal or vertical menu: Set to "h" or "v"
	classname: 'ddsmoothmenu', //class added to menu's outer DIV
	//customtheme: ["#1c5a80", "#18374a"],
	contentsource: "markup" //"markup" or ["container_id", "path_to_menu_file"]
})
*/
</script>

</head>
<body>


<div id="templatemo_wrapper">

<?
$user = 'Guest';
$logged_in = Yii::app()->user->getState('logged_in');
if($logged_in){
$profile = Yii::app()->user->getState('profile');
$user = $profile->name;
}
?>

<div class="navbar navbar-inverse nav" style="margin-bottom: 0px;">
    <div class="navbar-inner">
    <div class="container">
    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    </a>
    <a class="brand" href="/">HE Logo </a>
    <div class="nav-collapse collapse">
    <ul class="nav" >
    <li class="divider-vertical"></li>
    <!--<li><a href="#"><i class="icon-home icon-white"></i> </a></li>-->
    </ul>
    <div class="pull-right">
    
    <ul class="nav pull-right">
    <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <?=$user ?> <b class="caret"></b></a>
    <ul class="dropdown-menu" style="z-index: 1000;">
    <li><a href="/user/preferences"><i class="icon-cog"></i> Preferences</a></li>
    <li><a href="/help/support"><i class="icon-envelope"></i> Contact Support</a></li>
    <li class="divider"></li>
    <li><a href="/auth/logout"><i class="icon-off"></i> Logout</a></li>
    </ul>
    </li>
    </ul>
    </div>
    </div>
    </div>
    </div>
    </div>
  
	<!--   		<div id="templatemo_search">
            <form action="#" method="get">
              <input type="text" value="Search" name="keyword" id="keyword" title="keyword" onfocus="clearText(this)" onblur="clearText(this)" class="txt_field" length=100 />
              <input type="submit" name="Search" value="" alt="Search" id="searchbutton" title="Search" class="sub_btn"  />
            </form>
        </div>-->
<!-- end of top -->
    
    <div id="templatemo_header">
    	<div id="site_title"><a href="http://www.helixeffort.com">&nbsp;</a></div>
        <div id="templatemo_menu" class="ddsmoothmenu">
						<div align="center" style="font-size:15px;">Universal Worth</div>
						<div align="center" style="clear:both; font-size:35px; padding: 10px 10px;">1.05</div>
						<? if(!$logged_in){ ?>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('users/register'); ?>" style="font-size:15px;">Register as a Creator/User</a></div>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('auth/login'); ?>" style="font-size:15px;">Sign in</a></div>
						<? }else{ ?>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('itemDetails/create'); ?>" style="font-size:15px;">Add New Item</a></div>
						<div style="clear:both;" align="center"><a href="<?php echo $this->createUrl('itemDetails/index'); ?>" style="font-size:15px;">List Existing Items</a></div>
						<? } ?>
	</div>
	</div>	

<?php echo $content; ?>

    
    <div id="templatemo_footer">
    	<div class="col_4">
        	<h5>Quick Links:</h5>
            <ul class="footer_list">
            	<li><a href="#">FAQs</a></li>
                <li><a href="#">Theory</a></li>
                <li><a href="">Community</a></li>
                <li><a href="blog.html">Privacy Policy</a></li>
                <li><a href="contact.html">Disclaimers</a></li>
			</ul>
        </div>
        <div class="col_4">
        	<h5>&nbsp;</h5>
            <ul class="footer_list">
                <li><a href="#">Site Map</a></li>
            	<li><a href="#">helixeffort.org</a></li>
                <li><a href="#">Contact Us</a></li>
               <!-- <li><a href="#"></a></li>
                <li><a href="#"></a></li>-->
		</ul>             
        </div>
        <div class="col_4">
        	<h5>Twitter</h5>
            <ul class="twitter_post">
	            <li>Suspendisse at scelerisque urna. Aenean tincidunt massa in tellus varius. <a href="#">http://bit.ly/13IwZO</a></li>
                <li>Proin dignissim, diam nec enim lorem tempus orci, ac ante purus in justo scelerisque. <a href="#">http://bit.ly/34sdPo</a></li>
			</ul>
        </div>
        <div class="col_4 col_l">
        	<h5>Follow Us</h5>
            <p><a href="http://www.templatemo.com" target="_parent">Lorem ipsum</a> Suspendisse at scelerisque urna. Aenean tincidunt massa in tellus varius. <a href="#">In scelerisque</a> egestas est non convallis at nisi.</p>
            <div class="footer_social_button">
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/facebook-32x32.png" title="facebook" alt="facebook" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/flickr-32x32.png" title="flickr" alt="flickr" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/twitter-32x32.png" title="twitter" alt="twitter" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/youtube-32x32.png" title="youtube" alt="youtube" /></a>
                <a href="#"><img src="<?php echo $this->assetsBase ?>/images/rss-32x32.png" title="rss" alt="rss" /></a>
			</div>
		</div>
        <div class="cleaner"></div>
    </div>
    
</div> <!-- end of wrapper -->

<div id="templatemo_cr_bar_wrapper">
	<div id="templatemo_cr_bar">
    	© 2013 All rights reserved. <a href="http://www.helixeffort.com" target="_parent">Helixeffort.com</a>
    </div>
</div>
		<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery-easing-min.js"></script>
		<script type="text/javascript" src="<?php echo $this->assetsBase ?>/js/jquery.ExpandyShowcase.js"></script>
		<script type="text/javascript">
			$(window).load(function() {
				$('#expandy').expandyShowcase({
					'animationDuration' : 250,
					'slideEasing'		    : 'swing',
					'compressedSize'	  : '150px',
					'expandedSize'		  : '300px',
					'firstOpen'	    : 3
				});
			});
function clearText(field)
{
    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;
}
$(document).ready(function(){
$('.dropdown-toggle').dropdown();
});
</script>

</body>
</html>
