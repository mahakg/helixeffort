<h3>Will it affect the way my creations function?</h3>
<p>		
This actually depends. It depends on the business model philosophy a particular creator or his producer is operating upon. The way Immediate Media and hyperproducts function is one where there is no restriction to the way the audience is enabled to consume it. The ability of reach to the audience could be from anywhere on the internet whether streamed, downloaded, shared, borrowed or simply experienced elsewhere. This could be either purchased, subscribed or through other means with which the creator/producer had no control of. There are various economic factors as to why the creator loses control at that juncture. One is the ease that technology and infrastructure bring in that audiences seek any way possible to make it convenient for them to experience content. 
<p>
Following are most of the ways in which creators choose to monetize their works, either separately or as a combination of many:
<h4>Direct revenue</h4>
<ul>
<li>Retail purchase
<li>Virtual retail purchase
<li>Virtual digital purchase
<li>Pay per usage
<li>Pay for support/add-ons/feature
<li>Pay-what-you-want/can
<li>Donation
</ul>
<h4>Secondary revenue</h4>
<ul>
<li>Media events
<li>Merchandising
</ul>
<h4>Indirect revenue</h4>
<ul>
<li>Digital real estate
<li>Royalty
</ul>

<h4>Investments, capitals and funding </h4>

helixeffort tries to bring in a little of most of the above together thereby bringing stability to the way the audience perceives what they end up consuming. Along with this the service strongly suggests to retain the retail methods of propagation (retail-is-good philosophy) since retailing creations are the purest form of commerce that equates resources in a justifiable manner. While Immediate Media helps in navigating through problems of availability and accessibility in regions where physical products are hard to come by hyperproducts could impact the way physical products would function in places where retail plays a huge role. Negotiating through individual chosen business models is through the effort of the creators and the producers. The idea of copyright might have to be altered with the helixeffort business model (license – Phase II) which will state the fair usage of Immediate Media occurrences. Also the other players responsible for the retail production of products will be impacted to a certain extent and a thorough understanding of the implications have to be considered before choosing the helixeffort service (along with the T&C).
