<div class="container" style="background:white; border-radius:1px;">
        <div class="row">
  	<div class="span1 lb_col">
    	</div>
  	<div class="span9 lb_inner">
      
<div id="faq">
<h1 class="subh1">Frequently Asked Questions</h1>

<div class="sub_quote">
	<h2 class="sub_header">We've been making logos for so many years it's ridiculous.</h2>
</div>

<div class="row">
	<div class="span10">

		<h3>What's included in the package?</h3>
		<blockquote>
			First, you'll receive 10+ initial concept designs in B&amp;W.  After narrowing it down to your top 3, we will add color to the designs.  From there, you will choose your favorite and we'll provide you the opportunity to download.  In addition, we'll provide you with a link for unlimited access to your logo, for example if you want to share with others in your company. For your convenience, we'll store your logo indefinitely so you won't have to worry about losing it!
		</blockquote>

		<h3>When can I expect my finished logo?</h3>
		<blockquote>
			7 days. Projects can be extended due to revisions, but expect about a week. Although you may need your logo faster, we never want to deliver our clients rushed work. So be patient with us! (For more details, see our process page)
		</blockquote>


		<h3>Are all the designs original?</h3>
		<blockquote>
			Yup! Each and every design concept is 100% authentic and unique to your business. We pride ourselves in never using clip-art or stock icons, our logos are all products of our designers' imaginations!
		</blockquote>
		
		<h3>What formats can I receive my logo in?</h3>
		<blockquote>
			We are glad to supply you with the following formats:
			<ul>
			  <li>source EPS Vector file (also available in .ai and .pdf)</li>
			  <li>transparent PNG files for web use</li>
			  <li>JPG files for use with MS applications</li>
			</ul>
			and in addition, extra file types are available upon special request.
		</blockquote>
		
		<h3>What if I'm unhappy with the designs?</h3>
		<blockquote>
			If you're unhappy with the proposed designs, we encourage and welcome your feedback to revamp the designs to your liking. We'll give it some extra oomph! If you find you're still reluctant to the revisions, we'll give you half your money back. We must at least partially compensate our designers for the work they put in (no spec work here). We take pride in our work, and want our clients to be completely content with the finished product. Still unsure? Our clients rave about us. <a href="/testimonials">Read their testimonials.</a>
		</blockquote>
		
		<h3>What if I have my own idea for the logo design?</h3>
		<blockquote>Hey, we love more brain power! If you already have something in mind, please tell us about it in the initial form describing what your company does.  We will cater our designs around your idea.</blockquote>

		<h3>Which payment methods do you accept?</h3>
		<blockquote>Visa, MasterCard, American Express, Discover, JCB, and Diners Club cards.</blockquote>
		
		<h3>What if my question isn't listed?</h3>
		<blockquote>Not a problem! Drop us a line via our <a href="mailto:kingbro@logobro.com">email</a> or online form. We'll have our customer service team get back to you pronto!</blockquote>
    </div>
  </div>
</div>

    </div>
</div>

</div>
