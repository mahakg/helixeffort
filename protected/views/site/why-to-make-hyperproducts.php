<h3>Who can make hyperproducts and why would they make one?</h3>
<p>		
helixeffort is a conceptual service and allows the public to register and create a helixeffort ID and password. This enables anyone willing to, to make a hyperproduct out of a creative work or service when they think it is possible to make one. The reasons for making a hyperproduct are multiple, based on the user who intends to make one. However, the most primary reason would be to establish a means to support creators and establishers of content by paying them for their efforts directly while also giving the users the capacity of inducing a value to the worth of the content directly.
<p>
Below are few deciding characteristics of audience consuming and utilizing digital goods and services in a market economy and why they may not so easily take out their wallets to create a win-win situation. Because they:
<ul>
<li>don’t ‘need’ the product
<li>don’t ‘want’ the product (since they think they already have its content)
<li>don’t have the inclination to support the product/creator
<li>don’t have the capacity to support the product/creator
<li>don’t have reach to the product
</ul>
<p>
Hyperproducts generally can take care especially of the last two aspects. Hyperproducts can be made for situations when purchasing or subscription of real products is a more tedious affair. Especially when availability, accessibility and the ability to purchase becomes a crucial deciding factor for the audience.  This is more so the case for the audience coming from the developing and under-developed countries while still experiencing the power of the internet and various technologies and being subject to Immediate Media consumptions. In such cases, audiences can build (purchase) hyperproducts in the same way they would have visited a store (or an online store) and might have purchased a real product. 
<p>
Hyperproducts can also come of great use for content which has already been experienced in one way or the other without having the need or necessity to have purchased it. Today as more and more creations are being made free and unrestricted, and while business strategies are working around the concept of post-consumption, it suggests that convincing the audience is not as straight forward as it was back in the days. The average audience today is exposed to media a hundred times more than what it was even about a couple of decades ago. The need to purchase is continuously diminishing and providing reasons to buy is continuously increasing. With this situation the audience can then be simply allowed to make a hyperproduct of a particular song they might have come across through even a streaming service.
