<?php
$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>
<div class="hero-unit">
<h2>Error <?php echo $code; ?></h2>

<div class="error">
<?php echo $message; ?>
</div>
</div>
