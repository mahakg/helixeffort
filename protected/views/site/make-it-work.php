<h3>How do we make it work?</h3>
<p>		
<li>Individual consumes/utilizes a digital resource (maybe a book, an e-reading software, etc.) irrespective of how they have obtained it or how the experience has reached them
<li>Generates an impression
<li>Willingness to convert the impression into a recognizable/contributive form understanding that now there is an easier resource to do that
Logs into helixeffort
<li>Creates an hyperproduct* for the particular product/service/creator/participant by reimbursing a fee equivalent to the impression gathered from the experience guided by the ‘hyperproduct assessment tool’


<li>helixeffort records the hyperproduct into a Universal Worth system signifying change, contribution and participation in terms with a social scale enabling individuals to empower creations in their own ways 

<p>
*provided the creator registers onto the helixeffort service
